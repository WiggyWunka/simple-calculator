# Simple Java Calculator #

This project is a basic one. Not meant for everyday production (obviously :P) but is meant to be used to see how a  calculator would work.
### What is this repository for? ###

* Reference
* Basic Java practice
* To look cool

### How do I get set up? ###

* Download either the .java file or .jar file in the Releases section
* Compile the .java file using ```javac``` (if you downloaded the .java file) or run the .jar file via Command line
* ???
* Profit!

### How to contribute ###

If I missed something or spelled something wrong feel free to send a fix my way! Contributions are welcome!
